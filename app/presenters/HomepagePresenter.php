<?php

namespace App\Presenters;

use App\Model\PigLatinConvertor;
use App\Forms\DecodeFormFactory;
use App\Forms\EncodeFormFactory;

final class HomepagePresenter extends BasePresenter
{
    /** @var PigLatinConvertor */
    private $pigLatinConvertor;

    /** @var DecodeFormFactory */
    private $decodeFormFactory;
    
    /** @var EncodeFormFactory */
    private $encodeFormFactory;


    public function __construct(PigLatinConvertor $pigLatinConvertor, DecodeFormFactory $decodeFormFactory, EncodeFormFactory $encodeFormFactory) {
        parent::__construct();
        $this->pigLatinConvertor = $pigLatinConvertor;
        $this->decodeFormFactory = $decodeFormFactory;
        $this->encodeFormFactory = $encodeFormFactory;
    }
    
    public function actionDefault() {
//        bdump($this->pigLatinConvertor->encode('beast'));
//        bdump($this->pigLatinConvertor->encode('dough'));
//        bdump($this->pigLatinConvertor->encode('happy'));
//        bdump($this->pigLatinConvertor->encode('Question'));
//        bdump($this->pigLatinConvertor->encode('star'));
//        bdump($this->pigLatinConvertor->encode('three'));
//        bdump($this->pigLatinConvertor->encode('qusty'));
//        bdump($this->pigLatinConvertor->encode('queen'));
//        bdump($this->pigLatinConvertor->encode('quququtttaaa'));
//        bdump($this->pigLatinConvertor->encode('quyr'));
//        bdump($this->pigLatinConvertor->encode('yqutttaa'));
//        bdump($this->pigLatinConvertor->encode('yyqutr'));
//        bdump($this->pigLatinConvertor->encode('abbb'));
//        bdump($this->pigLatinConvertor->encode('trutrutru'));
//        bdump($this->pigLatinConvertor->encode('wa'));
//        bdump($this->pigLatinConvertor->encode('a'));
//        bdump($this->pigLatinConvertor->encode('wa', '-'));
//        bdump($this->pigLatinConvertor->encode('a', '-'));
//        bdump($this->pigLatinConvertor->encode('t'));
//        
//        
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('beast')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('dough')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('happy')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('Question')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('star')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('three')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('qusty')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('queen')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('quququtttaaa')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('quyr')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('yqutttaa')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('yyqutr')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('abbb')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('trutrutru')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('wa')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('a')));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('wa', '-'), '-'));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('a', '-'), '-'));
//        bdump($this->pigLatinConvertor->decode($this->pigLatinConvertor->encode('beast', '-'), '-'));
    }
    
    public function renderDefault()
    {
        $this->template->anyVariable = 'any value';
    }
    
    public function createComponentEncodeForm() {
        return $this->encodeFormFactory->create();
    }
    
    public function createComponentDecodeForm() {
        return $this->decodeFormFactory->create();
    }
}
