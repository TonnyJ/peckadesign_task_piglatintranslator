<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model\PigLatinConvertor;

final class EncodeFormFactory
{
	use Nette\SmartObject;
        
        /** @var FormFactory */
        private $formFactory;
        
        /** @var PigLatinConvertor */
        private $pigLatinConverter;

        public function __construct(FormFactory $formFactory, PigLatinConvertor $convertor) {
            $this->formFactory = $formFactory;
            $this->pigLatinConverter = $convertor;
        }

	/**
	 * @return Form
	 */
	public function create()
	{
		$form = $this->formFactory->create();
                
                $form->addText('word', 'English word:')->setRequired();
                $form->addText('encoded', 'Result:')->setDisabled();
                $form->addText('delimiter', 'Delimiter');
                
                $form->addSubmit('submit', 'Send');
                
                $form->onSuccess[] = [$this, 'onSuccess'];
                
		return $form;
	}
        
        public function onSuccess(Form $form) {
            $values = $form->values;
            $form['encoded']->setValue($this->pigLatinConverter->encode($values->word, $values->delimiter));
        }
}
