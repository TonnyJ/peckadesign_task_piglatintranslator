<?php

namespace App\Forms;

use Nette;
use Nette\Application\UI\Form;
use App\Model\PigLatinConvertor;

final class DecodeFormFactory
{
	use Nette\SmartObject;
        
        /** @var FormFactory */
        private $formFactory;
        
        /** @var PigLatinConvertor */
        private $pigLatinConverter;

        public function __construct(FormFactory $formFactory, PigLatinConvertor $convertor) {
            $this->formFactory = $formFactory;
            $this->pigLatinConverter = $convertor;
        }

	/**
	 * @return Form
	 */
	public function create()
	{
		$form = $this->formFactory->create();
                
                $form->addText('word', 'Pig-Latin word:')->setRequired();
                $form->addTextArea('decoded', 'Result:')->setDisabled();
                $form->addText('delimiter', 'Delimiter');
                
                $form->addSubmit('submit', 'Send');
                
                $form->onSuccess[] = [$this, 'onSuccess'];
                
		return $form;
	}
        
        public function onSuccess(Form $form) {
            $values = $form->values;
            $decoded = $this->pigLatinConverter->decode($values->word, empty($values->delimiter) ? ' ' : $values->delimiter);
            if (is_array($decoded)) {
                $form['decoded']->setValue(implode(';', $decoded));
            } else {
                $form['decoded']->setValue($decoded);
            }
            
        }
}
