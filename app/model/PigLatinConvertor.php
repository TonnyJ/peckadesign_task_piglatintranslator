<?php

namespace App\Model;

use Nette\Utils\Strings;

class PigLatinConvertor {
    const 
        ALPHA = '~([a-z]|[A-Z])+~',
        VOWELS = '~[aeiouy]~',
        NON_Y_VOWELS = '~[aeiou]~',
        SUFFIX = 'ay';

    /**
     * Check, if the value match with the preg
     * @param string $value String to verify
     * @param const $preg Regular expression for verify the string
     * @return bool TRUE if match
     */
    private function match(string $value, string $preg) {
        $matchedValue = Strings::match($value, $preg);
        return Strings::length($value) === Strings::length($matchedValue[0]);
    }
    
    /**
     * Gets Consonant prefix for given string
     * @param string $string String from we want to get prefix
     * @param bool $firstTime Flag indicates if this is first recursion floor
     * @return string Prefix for word
     */
    private function getConsonantPrefix(string $string, bool $firstTime = TRUE) {
        $prefix = '';
        
        if (Strings::startsWith(Strings::lower($string), 'qu')) {
            $prefix .= Strings::substring($string, 0, 2) . $this->getConsonantPrefix(Strings::substring($string, 2), FALSE);
        } else if ($firstTime && Strings::startsWith(Strings::lower($string), 'y')) {
            $prefix .= Strings::substring($string, 0, 1) . $this->getConsonantPrefix(Strings::substring($string, 1), FALSE);
        } else {
            $firstVowel = Strings::match($string, self::VOWELS)[0];
            $prefix .= Strings::before($string, $firstVowel);
        }
        
        return $prefix;
    }
    
    /**
     * Encode the given word to Pig-Latin
     * @param string $decodedWord String with only one word
     * @param string $delimiter Delimiter of strings to decode
     * @return string Encoded word OR original word if word is not valid
     */
    public function encode(string $decodedWord, string $delimiter = '') {
        if (!$this->match($decodedWord, self::ALPHA)) {
            return $decodedWord;
        }
        $firstVowel = Strings::match($decodedWord, self::VOWELS)[0];
        if ($firstVowel === NULL) {
            $retVal = $decodedWord . $delimiter . self::SUFFIX;
        } else {
            $prefix = $this->getConsonantPrefix($decodedWord);
            $retVal = Strings::after($decodedWord, $prefix) . $delimiter . $prefix . (empty($prefix) ? 'w' : '') . self::SUFFIX;
        }
        return $retVal;
    }
    
    private function innerDecode(string $word, int $depth = 1, array $decodedWords = []) {
        $strinLength = Strings::length($word) - $depth;
        if ($strinLength < 0) {
            return $decodedWords;
        }
        if ($strinLength === 0 && !Strings::contains(self::VOWELS, Strings::lower($word[$strinLength]))) {
            $decodedWords[] = $word;
        }
        if ($strinLength > 0) {
            if ($depth == 1 && Strings::contains(self::VOWELS, $word[0]) && !Strings::contains(self::VOWELS, $word[$strinLength])) {
                $decodedWords[] = Strings::substring($word, 0, $strinLength);
            }
            if (!Strings::contains(self::VOWELS, Strings::lower($word[$strinLength])) || ($depth == 1 && Strings::compare(Strings::lower($word[$strinLength]), 'y'))) {
                $depth += 1;
                $decodedWords[] = Strings::substring($word, $strinLength) . Strings::substring($word, 0, $strinLength);
                $decodedWords = $this->innerDecode($word, $depth, $decodedWords);
            } else if (Strings::endsWith(Strings::lower(Strings::substring($word, 0, $strinLength + 1)), 'qu')) {
                $depth += 2;
                $strinLength -= 1;
                $decodedWords[] = Strings::substring($word, $strinLength) . Strings::substring($word, 0, $strinLength);
                $decodedWords = $this->innerDecode($word, $depth, $decodedWords);
            }
        }
        return $decodedWords;
    }

    /**
     * Decode the given word from Pig-Latin
     * @param string $encodedWord String with only one word in Pig-Latin
     * @param string $delimiter Delimiter which is used to decode If '' then all possible variants are returned
     * @return string|array Decoded word OR original word if word is not valid
     */
    public function decode(string $encodedWord, string $delimiter = ' ') {
        $word = Strings::before($encodedWord, self::SUFFIX, -1);
        $words = explode($delimiter, $word);
        
        switch (count($words)) {
            case 1:
                $word = $words[0];
                if (!$this->match($word, self::ALPHA)) {
                    return $encodedWord;
                }
                
                return $this->innerDecode($word);
                break;
            case 2:
                $word1 = $words[0];
                $word2 = $words[1];
                if (!$this->match($word1, self::ALPHA) || !$this->match($word2, self::ALPHA)) {
                    return $encodedWord;
                }
                
                if (Strings::compare($word2, 'w')) {
                    return $word1;
                }
                
                return $word2 . $word1;
                break;
            default:
                return $encodedWord;
                break;
        }
        return $word;
    }
}
